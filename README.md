# Realtime Receipts API Example for Android

 A Simple example app for demonstrating how to get new receipts in real time from the SelfScanner JSON-RPC API.
 
### Usage ###
 
  * Start the App and go to https://my.selfscanner.net with your web browser.
  * Log in with username: hackathon, password: helsinki.
  * In the location OIH1, navigate to Cash Registers -> OIH1 -> USE and sell some products using "Debug Pay"
  * The receipts should appear in the TextView of this app in real-time